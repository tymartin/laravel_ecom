<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Product;
use App\Status; 
use Illuminate\Http\Request;
use Str;
use Auth;
use Session;

class TransactionController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
       $statuses = Status::all();
       // if(auth::user()->role_id==1){
       $transactions = Transaction::all();
        // } else 
        // {
        //   $transactions = Transaction::all()->whereIn('user_id', Auth::user()->id); 
        // }
       return view('transactions.index')
           ->with('transactions', $transactions)
           ->with('statuses', $statuses);
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       // Transaction table
           // Transaction code
           // user_id
       $transaction = new Transaction;
       $transaction->user_id = Auth::user()->id;
       $transaction->transaction_code = Auth::user()->id . Str::random(10);

       $transaction->save();

       // dd($transaction->id);

       // $cart = [
       //     id => qty,
       //     id => qty
       // ]

       $product_ids = array_keys(Session::get('cart'));
           // $product_ids = [id,id];
       $products = Product::find($product_ids);
       $total = 0;
       foreach ($products as $product){
           $product->quantity = Session::get("cart.$product->id");
           $product->subtotal = $product->price * $product->quantity;
           $total += $product->subtotal;

           $transaction->products()
               ->attach(
                   $product->id, 
                   [
                       "quantity" => $product->quantity, 
                       "subtotal" => $product->subtotal,
                       "price" => $product->price

                   ]
               );
       }

       $transaction->total = $total;
       $transaction->save();

       Session::forget("cart");

       // pivot table
           // product_id
           // transaction_id
           // quantity
           // subtotal

       return redirect( route('transactions.show',['transaction'=>$transaction->id])); 
     }

   /**
    * Display the specified resource.
    *
    * @param  \App\Transaction  $transaction
    * @return \Illuminate\Http\Response
    */
   public function show(Transaction $transaction)
   {
       $statuses = status::all(); 
       return view('transactions.show')
              ->with('transaction', $transaction)
              ->with('statuses', $statuses); 
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Transaction  $transaction
    * @return \Illuminate\Http\Response
    */
   public function edit(Transaction $transaction)
   {
       //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Transaction  $transaction
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, Transaction $transaction)
   {
     $transaction->status_id = $request->status;
     $transaction->save();
     return redirect( route('transactions.index')); 
   }

   public function destroy()
   {
    //
   }
   public function create_paypal_payment(Request $request)
   {
     $galing_sa_fetch = json_decode($request->getContent(), true); 
     return ["message" => $galing_sa_fetch];  
   }

  }