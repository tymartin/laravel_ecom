<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category; 

class CategoryController extends Controller
{   
	public function index() 
    { 
    	$categories = Category::all(); 
    	return view ("categories.index")->with('categories', $categories);
}
    public function create() 
    {
    	return view ("categories.create");
}
	public function show($id) 
    { 
    	$result = Category::find($id); 
    	return view('categories.show')->with('category', $result);
}

	public function store(Request $request)
	{
		$name = $request->input('name'); 
		$request->validate([
			'name' => 'required|unique:categories,name'

		]); 
 
			//$name=$request->input('name'); 

		$category = new Category; 
		$category->name = $request->input('name'); 
		$category->save();  
		
		return redirect('/categories');
	}
	 public function edit($id) 
    {
    	$result = Category::find($id); 
    	return view ("categories.edit")->with('category', $result);
} 
public function update(Category $id, Request $request) 
    {

    	$request->validate([
			'name' => 'required|unique:categories,name'

		]);

         //$result = Category::find($id); 

		//$request->input('name'); 

		// $result->save(); 

		$id->name = $request->input('name');

		$id->save();

		return redirect("categories/$id->id"); 
} 
public function destroy($id)
{
	$result = Category::find($id); 
	$result->delete(); 

	return redirect("/categories");
	
}
}
