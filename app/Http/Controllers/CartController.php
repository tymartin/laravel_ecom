<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Product;

class CartController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {

       if(Session::has('cart')) {
           // get all the id of cart session
           $product_ids = array_keys(Session::get('cart'));

           // query to the database
           // get all products that are in the session cart
           $products = Product::find($product_ids);

           $total = 0;
           foreach($products as $product) {
               $product->quantity = Session::get("cart.$product->id");
               $product->subtotal = $product->quantity * $product->price;
               $total += $product->subtotal;
           }

           // dd($products);

           return view('carts.index')
               ->with('products',$products)
               ->with('total',$total);
       } else {
           return view('carts.index');
       }
       // return "This is from carts index";

       
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       //
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
       //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
       // $cart = [
           // id => qty,
           // id => qty
       // ]
       $request->validate([
           'quantity' => 'required|min:1'
       ]);

       $qty = $request->quantity;
       $request->session()->put("cart.$id", $qty);
       
       return redirect( route('carts.index') );
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy(Request $request, $id)
   {
       $request->session()->forget("cart.$id");

       if (count($request->session()->get('cart')) == 0) 
       {
           $request->session()->forget("cart");
       }

       return redirect( route('carts.index'))->with('status', 'Product removed from cart');
    }
    public function empty()
    {
       Session::forget('cart');

       return redirect( route('carts.index'))->with('status','Cart Cleared');

   }
}