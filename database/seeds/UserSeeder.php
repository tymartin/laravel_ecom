<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash; 

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
       	'name' => 'Ace',
       	'email' => 'ace@email.com',
       	'password' => Hash::make('test1234'),
       ]);
        DB::table('users')->insert([
        'name' => 'Marty',
        'email' => 'mm@email.com',
        'password' => Hash::make('test1234'),
       ]);

    }
}
