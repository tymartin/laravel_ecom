<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
       	'name' => 'Samsung',
       	'price' => '14999.00',
       	'description' => 'Best Samsung in the world!',
       	'category_id' => 1,
       	'image' => 'products/k3RyuVpTyFipTZXCimr7t93afpknpg73ulTFxni1.jpeg'
		]);
       DB::table('products')->insert([
       	'name' => 'knight',
       	'price' => '599.00',
       	'description' => 'Ragnarok knight',
       	'category_id' => 2,
       	'image' => 'products/7Zb0nOtAxJKamZkBvYbdsf8DSG6pwQgmRU7m3ofR.png'
		]);
       DB::table('products')->insert([
       	'name' => 'Trainee Swordman',
       	'price' => '399',
       	'description' => 'Knight in Training',
       	'category_id' => 3,
       	'image' => 'products/m21xAjz5akj9ej1SnY8t5RG0OMzO3tf73wC6GSUO.jpeg'
       ]);
    }
}
