<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::delete('/carts/empty','CartController@empty')->name('carts.empty'); 

Route::post('/transactions/paypal','TransactionController@create_paypal_payment'); 

// Route::resource('categories', 'CategoryController'); 
	Route::get('/categories/create', 'CategoryController@create')->name('Categories.create'); 
	Route::get('/categories', 'CategoryController@index')->name('Categories.index'); 
	Route::post('/categories/create', 'CategoryController@store')->name('Categories.store'); 
	Route::get('/categories/{id}', 'CategoryController@show')->name('Categories.show'); 
	Route::get('categories/{id}/edit', 'CategoryController@edit')->name('Categories.edit'); 
	Route::patch('categories/{id}/', 'CategoryController@update')->name('Categories.update'); 
	Route::delete('/categories/{id}', 'CategoryController@destroy'); 

Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');


// Route::get('/products/create', 'ProductController@create'); 
// Route::get('/products', 'ProductController@index');

Route::resource('products', 'ProductController'); 
Route::resource('carts', 'CartController'); 
Route::resource('transactions', 'TransactionController'); 







// 	Route::get('/products/create', function () {
//     return view('Products.create');
// });

