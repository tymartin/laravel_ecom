
@extends('layouts.app')
@section('content')

	<div class="container">

 <div class="row">
			<div class="col-12">
				<form action="" method="get">
				@csrf
					<div class="row">
					<div class="col">
						<select name="category" id="category" class="form-control">
							<option value="">All</option>
								@foreach($categories as $category)
								<option value="{{ $category->id }}">{{ $category->name }}</option>
								@endforeach
							</select>
							</div>
							<div class="col">
						<button class="btn btn-outline-primary">Filter</button>
						</div>
					</div>
				</form>
				</div>
			</div>
<div class="row">


		<div class="row">

			@foreach($products as $product)
			{{-- start of cards --}}
				<div class="col-12 col-md-4 col-lg-3">
					<div class="card">
						<img src="/storage/{{ $product->image}}" alt="..." class="card-img-top">
							<div class="card-body">
								<h5 class="card-title">{{ $product->name }}</h5>
									<p class="card-text">&#8369; {{ number_format($product->price,2) }}</p>
									<p class="card-text">{{ $product->category->name }}</p>
									<p class="card-text">{{ $product->description }}</p>
							</div>
									<div class="card-footer">
									<form action="{{ route('carts.update', ['cart'=>$product->id])}}" method="post">
											@method('PUT')
											@csrf
										<input type="number" name="quantity" id="quantity" class="form-control" mb-1 min="1">
										<button class="btn btn-outline-primary w-100 mb-2">Add to Cart</button>
									</form>
										<a href="{{ route('products.show',['product' => $product->id])}}" class="btn btn-outline-success w-100 mb-2">View Product</a>
										<a href="{{ route('products.edit',['product' => $product->id])}}" class="btn btn-outline-warning w-100 mb-2">Edit Product</a>

										<form action="{{ route('products.destroy',['product' => $product->id])}}" method="post">
										@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-outline-danger w-100 mb-2">Delete Product</button>
							</form>
						</div>
					</div>
				</div>
		{{-- end of cards --}}
		@endforeach
		</div>
	</div>
@endsection