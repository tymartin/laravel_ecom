@extends('layouts.app')
	@section('content')

@if($product)

	<div class="container">
		<div class="row">
			{{-- start of cards --}}
				<div class="col-12 col-md-4 col-lg-3">
					<div class="card">
						<img src="/storage/{{ $product->image}}" alt="..." class="card-img-top">
							<div class="card-body">
								<h3> {{ $product->name }}</h3>
								<p class="card-text">&#8369; {{ number_format($product->price,2) }}</p>
									<p class="card-text">{{ $product->category->name }}</p>
									<p class="card-text">{{ $product->description }}</p>
							</div>

							<div class="card-footer">
										<form action="{{ route('carts.update', ['cart'=>$product->id])}} method="post">
											@csrf
											@method('PUT')
											<input type="number" name="quantity" id="quantity" class="form-control" mb-1 min="1">
											<button class="btn btn-outline-primary w-100 mb-2">Add to Cart</button>
										</form>


											<a href="{{ route('products.edit',['product' => $product->id])}}" class="btn btn-outline-warning w-100 mb-2">Edit Product</a>

										<form action="{{ route('products.destroy',['product' => $product->id])}}" method="post">
										@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-outline-danger w-100 mb-2">Delete Product</button>
							</form>

							</div>



								@else
								 
								 	404 not Found 

								@endif 

					</div>
				</div>
		</div>
	</div>




@endsection