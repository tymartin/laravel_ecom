@extends('layouts.app')
@section('content')
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-8 mc-auto">
					<h3>Create Product Form</h3>
						<hr>
						<form action="{{ route('products.store')}}" method="post" enctype="multipart/form-data">
						@csrf

								<input type="text" name="name" id="name" class="form-control mb-2" placeholder="Product Name" value="{{ old('name') }}">
								@if($errors->has('name'))
								<div class="alert alert-danger">
								{{ $errors->first('name')}}
								</div>
								@endif

								<input type="text" name="price" id="price" class="form-control mb-2" placeholder="Price" value="{{ old('price') }}">
								@if($errors->has('price'))
								<div class="alert alert-danger">
								{{ $errors->first('price')}}
								</div>
								@endif

								<select name="category-id" id="category-id" class="custom-select mb-2">
								@foreach($categories as $category)
								<option value="{{ $category->id }}" 
								{{ old('category-id') == $category->id ? "selected" : ""}}>
								{{ $category->name }}
								</option>
								@endforeach
								</select>
								@if($errors->has('category-id'))
								<div class="alert alert-danger">
								{{ $errors->first('category-id')}}
								</div>
								@endif

								<input type="file" name="image" id="image" class="form-control-file mb-2">
								@if($errors->has('image'))
								<div class="alert alert-danger">
								{{ $errors->first('image')}}
								</div>
								@endif

								<textarea name="description" id="description" class="form-control mb-3" cols="10" rows="5" placeholder="Product Description">{{ old('description') }}</textarea>
								@if($errors->has('description'))
								<div class="alert alert-danger">
								{{ $errors->first('description') }}
								</div>
								@endif

		<button type="submit" class="btn btn-primary">Create New Product</button>
		</form>
		</div>
		</div>
</div>
@endsection

