@extends('layouts.app')
@section('content')
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-8 mx-auto">
					<h3>Edit Product Form</h3>
						<hr>
						@if(Session::has('status'))
							<div class="alert alert-success">{{ Session::get('status')}}</div>
						@endif
						<form action="{{ route('products.update', ['product'=>$product->id])}}" method="post" enctype="multipart/form-data">
						@csrf
						@method('PATCH')

								<input type="text" name="name" id="name" class="form-control mb-2" placeholder="Product Name" value="{{ $product->name }}">
								@if($errors->has('name'))
								<div class="alert alert-danger">
								{{ $errors->first('name')}}
								</div>
								@endif

								<input type="text" name="price" id="price" class="form-control mb-2" placeholder="Price" value="{{ $product->price }}">
								@if($errors->has('price'))
								<div class="alert alert-danger">
								{{ $errors->first('price')}}
								</div>
								@endif

								<select name="category-id" id="category-id" class="custom-select mb-2">
								@foreach($categories as $category)
								<option value="{{ $category->id }}" 
								{{ old('category-id') == $category->id ? "selected" : ""}}>
								{{ $category->name }}
								</option>
								@endforeach
								</select>
								@if($errors->has('category-id'))
								<div class="alert alert-danger">
								{{ $errors->first('category-id')}}
								</div>
								@endif

								<input type="file" name="image" id="image" class="form-control-file mb-2" value="{{ $product->image }}">
								@if($errors->has('image'))
								<div class="alert alert-danger">
								{{ $errors->first('image')}}
								</div>
								@endif

								<textarea name="description" id="description" class="form-control mb-3" cols="10" rows="5" placeholder="Product Description">{{ $product->description }}</textarea>
								@if($errors->has('description'))
								<div class="alert alert-danger">
								{{ $errors->first('description') }}
								</div>
								@endif

		<button type="submit" class="btn btn-success">Save Updates</button>
		</form>
		</div>
			<div class="col-12 col-md-4 mx-auto">
					<img src="/storage/{{ $product->image}}" alt="..." class="card-img-top">
							<div class="card-body">
								<h3> {{ $product->name }}</h3>
								<p class="card-text">&#8369; {{ number_format($product->price,2) }}</p>
									<p class="card-text">{{ $product->category->name }}</p>
									<p class="card-text">{{ $product->description }}</p>
							</div>

			</div>
		</div>

</div>
@endsection

