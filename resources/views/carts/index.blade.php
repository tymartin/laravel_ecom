@extends('layouts.app')


@section('content')
<script src="https://www.paypal.com/sdk/js?client-id=AQSmcK2Aoknk-WOBddqWktY2nz5YlYGF6ntu8s0FjVIi0e2xyupAZACbSAqi3OFSsI05dmDKUzFq9Phj"></script>
{{-- 	{{ dd($request ?? ''->session()->get('cart'))}} --}}
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>My Cart</h3>
			</div>
			@if(Session::has('status'))
				<div class="col-12">
					<div class="alert alert-success">
						{{ Session::get('status')}}
					</div>
				</div>
			@endif

			
		@include('products.errors')

			@if(Session::has('cart'))
			

		{{-- 			table --}}
			<div class="col-12">
				<div class="table table-responsive"> 
					<table class="table table-striped table-hover text-center">
								<thead>
									
									<th scope="col">Name</th>
									<th scope="col">Price per Unit</th>
									<th scope="col">Quantity</th>
									<th scope="col">Subtotal</th>
									<th scope="col">action</th>
								</thead>
												<tbody>
														@foreach($products as $product)
							{{-- start of row --}}
																	<tr>
																		
																		<th scope="row">{{ $product->name }}</th>
																		<td>&#8369; <span> {{ $product->price }}</span></td>
																		<td>
																			<div class="add-tocart-field mb-1">
																				<form action="{{ route('carts.update',['cart'=>$product->id])}}" method="post">
																					@csrf
																					@method('PUT')
																					<input
																						type="number"
																						name="quantity"
																						id="quantity"
																						class="form-control"
																						value="{{$product->quantity}}"
																						min="1"
																					>
																					<button
																						class="btn btn-outline-secondary w-100"
																						type-"submit"
																					>Edit
																					</button>
																				</form>
																			</div>

																		</td>

																				<td>
																					&#8369; {{ number_format($product->subtotal,2) }}
																				</td>
																				<td>
																	{{-- 	<form action="{{route('carts.destroy'), ['carts'=> $product->id]}}" method="post">
																						@csrf
																						@method('DELETE')
																						
																					<button class="btn btn-danger w-100">Remove From Carts
																					</button>
																				</form> --}}
																				</td>
																
																    </tr>
																@endforeach
												</tbody> 
																	<tfoot>
																			<tr>
																				
																				<td colspan="3" class="text-right">Total</td>
																				<td>&#8369; {{ number_format($total ?? '' ?? '',2) }} </td>	
																				 <td>
																							<form action="{{ route('transactions.store')}}" method="post">
																							
																							@csrf
																							<button class="btn btn-primary w-100">Checkout</button>
																							</form>

																							<div id="paypal-btn"></div>
																							</td>
																			</tr>
																	</tfoot>
								</table>
							</div>
									<form action="{{ route('carts.empty')}}" method="post">	
											@csrf
											@method('DELETE')
	                                     <button type="submit" class="btn btn-outline-danger">Clear Cart</button>
                                 	</form>
               </div>
               @else
               	<div class="col-12">
               		<div class="alert alert-info">
               			Cart Empty
               		</div>
               	@endif 
               	</div>
		</div>
	</div>



<script>
paypal.Buttons({
   createOrder: function(data, actions) {
     // This function sets up the details of the transaction, including the amount and line item details.
     return actions.order.create({
       purchase_units: [{
         amount: {
           value: {{ $total ?? '' }}
         }
       }]
     });
   },
   onApprove: function(data, actions) {
     // This function captures the funds from the transaction.
     return actions.order.capture().then(function(details) {
       // This function shows a transaction success message to your buyer.
       alert('Transaction completed by ' + data.orderID);
       let csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

       let data = {transactionCode : data.orderID}

       fetch("route for paypal payment",{
       	method : 'post',
       	body: JSON.stringify(data),
       	headers : { 'X-CSRF-TOKEN' : csrfToken }
       })
       .then( response=> response.json())
       .then (res => console.log(res)); 
   });
   }
 }).render('#paypal-btn');
</script>

@endsection