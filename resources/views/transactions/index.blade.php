@extends('layouts.app')

@section('content')


<div class="container">
	<div class="row">
		<div class="col-12">
		<h3>My Transactions</h3>
		</div>
	</div>

		<div class="col-12">
		<div class="accordion" id="accordionExample">
		{{-- start of accordion --}}
		@foreach ($transactions as $transaction)
		
		<div class="card">
		<div class="card-header" id="headingOne">
		<h2 class="mb-0">
		<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#transaction-{{ $transaction->id }}" aria-expanded="true" aria-controls="collapseOne">
			
		<h4 class=" float-right w-100">
		{{ $transaction->created_at->format('F d,Y') }}
		</h4>
		<span class="badge badge-info float-right w-100">{{ $transaction->status->name }}</span>




		</button>
		</h2>
		</div>
		<div id="transaction-{{ $transaction->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		<div class="card-body">
		{{-- start of table --}}
		<div class="table-responsive">
		{{-- start of transaction table --}}
		@include('transactions.includes.table-transaction')
		{{-- end of table product_transaction --}}
		</div>
		{{-- end of table --}}
		</div>
		</div>
		</div>
		@endforeach
		{{-- end of accordion --}}
		</div>
	</div>
</div>





@endsection