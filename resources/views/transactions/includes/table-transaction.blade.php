<table class="table table-striped">
	<tbody>

		<tr>
			<td>Customer Name:</td>
			<td>{{ $transaction->user->email }}</td>
		</tr>


		<tr>
			<td>
			Transaction Code
			</td>
			<td>
			{{ $transaction->transaction_code }}
			</td>
		</tr>

		<tr>
			<td>
			Payment mode:
			</td>
			<td>
			{{ $transaction->payment_mode->name}}
			</td>
		</tr>

		<tr>
			<td>
			Date of Purchase
			</td>
			<td>
			{{ $transaction->created_at->format('F d, Y h:i:s') }}
			</td>
		</tr>

		<tr>
			<td>
			Status
			</td>
		<td>
				{{ $transaction->status->name }}

				<form action="{{ route('transactions.update', ['transaction'=>$transaction->id])}}" method="post">
				@csrf
				@method('PUT')

				<select name="status" id="status" class="form-control">

					@foreach($statuses as $status)
						<option value="{{$status->id}}" {{$transaction->status_id == $status->id ? 'selected':''}}>
						{{$status->name}}
						</option>
					@endforeach

				</select>

			<button class="btn btn-warning">Edit Status</button>
			</form>

		</td>
	</tr>
	</tbody>
</table>
<hr>
{{-- end of transaction table --}}

{{-- {{dd($transaction->products)}} --}}

{{-- start of table product_transaction --}}
	<table class="table table-striped table-hover">

	<thead>
		<th scope="row">Product Name</th>
		<th scope="row">Price</th>
		<th scope="row">Quantity</th>
		<th scope="row">Subtotal</th>
	</thead>

<tbody>
{{-- start of product-transaction details --}}
		@foreach($transaction->products as $transaction_product)
				<tr>
					<td>
					{{$transaction_product->name}}
					</td>

					<td>
					&#8369; {{ number_format($transaction_product->pivot->price,2) }}
					</td>

					<td>
					{{$transaction_product->pivot->quantity}}
					</td>

					<td>
					&#8369; {{ number_format($transaction_product->pivot->subtotal,2) }}
					</td>
				</tr>
		@endforeach
{{-- end of product-transaction details --}}
</tbody>

	<tfoot>
		<tr>
			<td colspan="3" class="text-right"><strong>Total</strong></td>
			<td>&#8369; {{ number_format($transaction->total,2) }}</td>
		</tr>
	</tfoot>
</table>
