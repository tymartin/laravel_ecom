@extends('layouts.app')
@section('content')



<h1>Create new Category Here:</h1>
@if($errors->any())
	<div>
		@foreach($errors->all() as $error)
			<p> <strong> {{ $error }}</strong> </p>
		@endforeach
	</div>
@endif 
<form action="/categories/create" method="post">
		
@include('categories.form')

		




</form>



 <a href="/categories">Back</a>

 @endsection 
 