<h1> Edit Category</h1>


@if($errors->any())
	<div>
		@foreach($errors->all() as $error)
			<p> {{ $error }}</p>
		@endforeach
	</div>
@endif 
<form action="/categories/{{ $category->id }}" method="post">
		


@method('PATCH')


	<label for="name" >Category Name:</label>
	<input type="text" name="name" value={{ $category->name }}>
	<button type="submit">Save Updates</button>

@csrf




</form>
		
<a href="/categories">Back</a>